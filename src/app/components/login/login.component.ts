import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  signIn() {
    this.router.navigateByUrl('page-1');
  }

  /*convertToTitleCaseInput(event) {

    console.log(event.key);
    let letter = event.key;
    console.log(letter && letter !== '' && letter.length == 1 && this.onlyLetters(letter));
    if (letter && letter !== '' && letter.length == 1 && this.onlyLetters(letter)) {
      event.preventDefault();
      event.target.value = this.convertToTitleCase(event.target.value + event.key);
    } else if (letter && letter !== '' && letter.length == 1) {
      event.preventDefault();
    }

    
  }

  convertToTitleCase(event){
    let data = event.target.value;
    data = data.toLowerCase().split(' ');
    for (var i = 0; i < data.length; i++) {
      data[i] = data[i].charAt(0).toUpperCase() + data[i].slice(1);
    }
    console.log("data.join(' ')", data.join(' '));
    data = data.join(' ');
    event.target.value = data;
    return data;
  }

  onlyLetters(str) {
    if (str.match("^[0-9a-zA-Z]+$")) {
      return true;
    } else {
      return false;
    }
  }*/

}
