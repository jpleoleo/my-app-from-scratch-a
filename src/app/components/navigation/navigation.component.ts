import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  navigateTo(page:string){
    this.router.navigateByUrl(page);
  }

}
